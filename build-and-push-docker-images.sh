#!/bin/bash
set -e

docker build --no-cache --force-rm \
-t registry.gitlab.com/pleio/matomo:latest \
-t registry.gitlab.com/pleio/matomo:$(git rev-parse --short HEAD) \
.

docker push registry.gitlab.com/pleio/matomo:latest
docker push registry.gitlab.com/pleio/matomo:$(git rev-parse --short HEAD)
