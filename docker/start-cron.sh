#!/bin/sh

echo "[i] Initializing environment..."
/initialize.sh

echo "[i] Starting daemon..."
/usr/sbin/cron -f
