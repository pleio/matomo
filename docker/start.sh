#!/bin/sh

echo "[i] Initializing environment..."
/initialize.sh

echo "[i] Starting daemon..."
apache2-foreground
