#!/bin/sh

# Get latest GeoIP database
RUN wget "https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-City&license_key=g34L41v6S0nZgZ9V&suffix=tar.gz" -O temp.tar.gz && \
    tar -xzf temp.tar.gz -C /app/misc --strip-components 1 && \
    rm temp.tar.gz

envsubst < /app/docker/msmtp.conf > /etc/msmtprc
envsubst < /app/docker/config.ini.php > /app/config/config.ini.php
