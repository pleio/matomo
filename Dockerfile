FROM php:7.4-apache

# Request system packages
RUN apt-get update && apt-get install --no-install-recommends -y \
    libxml2-dev \
    libpng-dev \
    libjpeg-dev \
    libcurl4-openssl-dev \
    cron \
    wget \
    gzip \
    tar \
    diffutils \
    msmtp \
    gettext \
&& rm -rf /var/lib/apt/lists/*

# PHP extensions
RUN docker-php-ext-install \
    pdo \
    pdo_mysql \
    xml \
    curl \
    gd \
&& docker-php-source delete

# Apply security patches
RUN apt-get update && apt-get upgrade -y && apt-get dist-upgrade -y


# Remove development packages
RUN apt-get update && apt-get remove -y linux-libc-dev \
  libxml2-dev \
  libpng-dev \
  libjpeg-dev \
  libcurl4-openssl-dev 

# PHP configuration
COPY ./docker/php.ini /usr/local/etc/php/php.ini

#  Scripts
COPY ./docker/start.sh /start.sh
COPY ./docker/start-cron.sh /start-cron.sh
COPY ./docker/initialize.sh /initialize.sh
COPY ./docker/initialize-cron.sh /initialize-cron.sh
COPY ./docker/archive.sh /archive.sh
COPY ./docker/crond-matomo /etc/cron.d/matomo
RUN chmod +x /start.sh /start-cron.sh /initialize.sh /initialize-cron.sh /archive.sh /etc/cron.d/matomo

# Get Matomo
RUN wget https://builds.matomo.org/matomo-3.13.1.tar.gz -O temp.tar.gz && \
    mkdir /build && \
    tar -xzf temp.tar.gz -C /build && \
    mv /build/matomo /app && \
    rm temp.tar.gz && \
    rm -Rf /build

# Additional configuration
COPY ./docker/000-default.conf /etc/apache2/sites-available
COPY ./docker/msmtp.conf /app/docker/msmtp.conf
COPY ./docker/config.ini.php /app/docker/config.ini.php
COPY ./docker/remoteip.conf /etc/apache2/conf-available/remoteip.conf

# Set permissions
RUN chown -R www-data:www-data /app/tmp /app/piwik.js /app/js

# Apache modules
RUN a2enmod rewrite remoteip
RUN a2enconf remoteip

WORKDIR /app

# Define run script
CMD ["/start.sh"]
